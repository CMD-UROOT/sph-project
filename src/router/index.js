//配置路由的地方
import Vue from 'vue'
import VueRouter from 'vue-router'
//使用插件
Vue.use(VueRouter)
//引入routes.js
import routes from './routes'
//引入store
import store from '@/store'

//先把VueRouter原型对象的push保存一份
let originPush = VueRouter.prototype.push;
// console.log(originPush)打印可以看到VueRouter的原型的push方法
let originReplace = VueRouter.prototype.replace

// 重写push
// 第一个参数:告诉原来push方式, location你往哪来跳转(传递哪些参数) 
// 第二个参数:成功的回调
// 第三个参数:失败的回调
// call||apply区别
// 相同点:都可以调用函数一次,都可以篡改函数的上下文一次
// 不同点:call与apply传递参数:call传递参数用逗号隔开,apply方法执行,传递数组 
VueRouter.prototype.push = function (location,resolve,reject) {
  if(resolve && reject){  //如果你成功失败的回调都传了
    originPush.call(this,location,resolve,reject)
  } else { //否则,成功失败回调传其一,或者都不传的情况
    originPush.call(this,location,()=>{},()=>{})
  }
}

// 重写replace
VueRouter.prototype.replace = function(location,resolve,reject){
  if(resolve && reject){
    originReplace.call(this,location,resolve,reject)
  } else {
    originReplace.call(this,location,()=>{},()=>{})
  }
}

//对外暴露 VueRouter类的实例
let router =  new VueRouter({
  //正常写应该是routes=routes,因为k,v一致省略v,这里直接写routes就可以了
  routes,
  //滚动行为
  scrollBehavior(to, from, savedPosition) {
    // 返回的这个y=0,代表的滚动条在最上方
    return { y: 0 }
  },
})

//全局守卫:前置守卫(在路由跳转之前进行判断)
router.beforeEach(async(to,from,next)=>{
  //to:可以获取到你要跳转到哪个路由信息
  //from:可以获取到你从哪个路由信息而来的信息
  //next:放行函数 next()放行 next(path)放行到指定路由  next(false)
  // console.log(to,from,next)
  //为了测试先全都放行
  //next()
  // console.log(store)
  //用户登录了,才会有token,未登录一定不会有token
  let token = store.state.user.token
  //用户信息  用名字来判断放行之前我们要确保name得有,有才放行
  let name = store.state.user.userInfo.name
  //用户已经登录了
  if(token){
    //用户已经登录了,还想去login[不行,停留在首页]
    if(to.path=='/login'||to.path=='/register'){
      next('/home')
    } else {
      //登录了,访问的是非登录与注册
      //如果用户名已有  [放行]
      if(name){
        next()
      }else{
        //登录了,且没有用户信息,派发action让仓库存储用户信息再跳转
        try {
          //获取用户信息成功  [放行]
          await store.dispatch('getUserInfo')
          next()
        } catch (error) {
          //token失效了获取不到用户信息,重新登录 
          //清除token
          await store.dispatch('userLogout')
          next('/login')
        }
      }     
    } 
  } else {
    //未登录:不能去交易相关、不能去支付相关[pay|paysuccess]、不能去个人中心
    // console.log(to.path)
    // 未登录去上面这些路由----应该跳到登录页面
    let toPath = to.path
    // =-1就是不包含,!=-1就是包含/trade,就跳转到/login,因为路径都是字符串,所以用indexOf来判断
    //支付相关[pay|paysuccess]里面都有pay,这里写包含pay就能判断,一举两得
    if(toPath.indexOf('/trade')!=-1 || toPath.indexOf('/pay')!=-1 || toPath.indexOf('/center')!=-1){ 
      //把未登录的时候想去而没有去成的信息,存储于地址栏当中[路由]
      next('/login?redirect='+toPath)
    }else{
      // 去的不是上面的路由(home|search|shopCart)----放行
      next()
    }
  }
})

export default router


