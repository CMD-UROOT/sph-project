/*
当打包构建应用时，JavaScript 包会变得非常大，影响页面加载。
如果我们能把不同路由对应的组件分割成不同的代码块，然后当路由被访问的时候才加载对应组件，这样就更加高效了。
*/
//路由配置信息
export default [
  {
    path: '/center',
    component: () => import('@/pages/Home'),
    meta:{show:true},
    //二级路由组件
    children:[
      {
        path:'myorder',
        component: () => import('@/pages/Center/myOrder'),
      },
      {
        path:'grouporder',
        component: () => import('@/pages/Center/groupOrder'),
      },
      {
        path:'/center',  //如果访问/center,重定向到/center/myorder
        redirect:'/center/myorder' //路由重定向
      }
    ]
  },
  {
    path: '/paysuccess',
    component: () => import('@/pages/PaySuccess'),
    meta:{show:true}
  },
  {
    path: '/pay',
    component: () => import('@/pages/Pay'),
    meta:{show:true},
    //路由独享守卫
    beforeEnter: (to, from, next) => {
      //去支付页面,必须是从交易页面而来
      if(from.path=="/trade"){
        next()
      }else{
        //其他的路由组件而来,停留在当前
        next(false)
      }
    }
  },
  {
    path: '/trade',
    component: () => import('@/pages/Trade'),
    meta:{show:true},
    //路由独享守卫
    beforeEnter: (to, from, next) => {
      //去交易页面,必须是从购物车而来
      if(from.path=="/shopcart"){
        next()
      }else{
        //其他的路由组件而来,停留在当前
        next(false)
      }
    }
  },
  {
    path: '/shopCart',
    component: () => import('@/pages/ShopCart'),
    meta:{show:true}
  },
  {
    path: '/addCartSuccess',
    name: 'addCartSuccess',
    component: () => import('@/pages/AddCartSuccess'),
    meta:{show:true}
  },
  {
    path: '/detail/:skuid',
    component: () => import('@/pages/Detail'),
    meta:{show:true}
  },
  {
    path: '/home',
    component: () => import('@/pages/Home'),
    meta:{show:true}
  },
  {
    path: '/search/:keyword?',
    component: () => import('@/pages/Search'),
    meta:{show:true},
    name:"search",
    //面试题4:路由组件能不能传递props数据?
    //布尔值写法:只有params参数支持
    // props:true,
    //对象写法:额外的给路由组件传递一些props
    // props:{a:1,b:2}
    //函数写法:可以把params参数、query参数,通过props传递给路由组件
    props:($route)=>({keyword:$route.params.keyword,k:$route.query.k})
  },
  {
    path: '/login',
    component: () => import('@/pages/Login'),
    meta:{show:false}
  },
  {
    path: '/register',
    component: () => import('@/pages/Register'),
    meta:{show:false},
  },
  //重定向,在项目跑起来的时候,访问/,立马让他定向到首页
  {
    path: '*',
    redirect: "/home"
  },
  {
    path: '/communication',
    component: () => import('@/pages/Communication/Communication'),
    children: [
      {
        path: 'event',
        component: () => import('@/pages/Communication/EventTest/EventTest'),
        meta: {
        show: false
        },
      },
      {
        path: 'model',
        component: () => import('@/pages/Communication/ModelTest/ModelTest'),
        meta: {
            show: false
        },
      },
      {
        path: 'sync',
        component: () => import('@/pages/Communication/SyncTest/SyncTest'),
        meta: {
            show: false
        },
      },
      {
        path: 'attrs-listeners',
        component: () => import('@/pages/Communication/AttrsListenersTest/AttrsListenersTest'),
        meta: {
            show: false
        },
      },
      {
        path: 'children-parent',
        component: () => import('@/pages/Communication/ChildrenParentTest/ChildrenParentTest'),
        meta: {
            show: false
        },
      },
      {
        path: 'scope-slot',
        component: () => import('@/pages/Communication/ScopeSlotTest/ScopeSlotTest'),
        meta: {
            show: false
        },
      }
    ],
  },
]