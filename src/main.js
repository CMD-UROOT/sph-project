import Vue from 'vue'
import App from './App.vue'

//定义全局组件:在入口文件注册一次之后,在任何组件当中都可以使用
//三级联动的组件---全局组件 
import TypeNav from '@/components/TypeNav'
//轮播图的组件---全局组件
import Carousel from '@/components/Carousel'
//分页器的组件--全局组件
import Pagination from '@/components/Pagination'
import { Button,MessageBox } from 'element-ui';

//全局组件: 第一个参数:全局组件的名字  第二个参数:哪一个组件
Vue.component(TypeNav.name,TypeNav)
Vue.component(Carousel.name,Carousel)
Vue.component(Pagination.name,Pagination)
Vue.component(Button.name,Button)
//ElementUI注册组件的时候,还有一种写法,挂在原型上
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
//引入路由
import router from './router'

//引入仓库
import store from './store'

//引入MockServe.js
import '@/mock/mockServe'

//引入Swiper样式
import "swiper/css/swiper.css"

//统一接口api文件夹里面全部请求函数
// * as统一引入的意思
import * as API from '@/api'  
// console.log(API) //现在的API是一个对象,对象包含api/index.js中所有的请求接口

// import {reqGetSearchInfo} from '@/api'
// console.log(reqGetSearchInfo({})) //调用params,这里至少括号中是个空对象({}),才能拿到数据

// //测试
// import {reqCategoryList} from '@/api'  //引入api/index.js中我们封装的函数
// reqCategoryList()  //函数调用,去控制台查看

//引入lazyload插件
import VueLazyload from 'vue-lazyload'
import hyrz from '@/assets/1.gif' //引入gif图
//注册lazyload插件
Vue.use(VueLazyload,{
  //懒加载默认的图片
  loading:hyrz
})

Vue.config.productionTip = false

//引入自定义插件
import myPlugins from '@/plugins/myPlugins'
//安装插件
Vue.use(myPlugins,{
  name:'upper'
})

//引入表单校验插件
import "@/plugins/validate"

new Vue({
  //全局事件总线$bus配置
  beforeCreate(){
    Vue.prototype.$bus = this //这里this是VM
    Vue.prototype.$API = API  //所有请求的接口统一接收到以后,而且挂载在Vue.prototype原型对象身上,这样的好处是所有的组件都不需要在一个一个引入了,我们可以直接找这个$API对象直接用
  },
  //注册路由
  //注册路由信息:当这里书写router的时候,组件身上都拥有$route,$routers属性
  router,
  //注册仓库: 组件实例的身上会多一个属性$store属性
  store,
  render: h => h(App)
}).$mount('#app')
