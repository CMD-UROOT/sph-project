//对axios进行二次封装

//在当前模块中引入store
import store from '@/store'
//引入axios
import axios from "axios";
//引入进度条
import nprogress from "nprogress";  //具体使用方法可以翻看gitee文档获取npm社区提供的文档,就是去百度嘛
// console.log(nprogress) //打印后可以在控制台看到nprogress中的start和done属性
//nprogress中的 start:进度条开始 done:进度条结束
//引入进度条样式
import "nprogress/nprogress.css";


//1:利用axios对象的方法create,去创建一个axios实例
//2:requests就是axios,只不过稍微配置一下
const requests = axios.create({
  //配置对象
  //基础路径,发请求的时候,路径当中会出现/api
  baseURL:"/api",
  //代表请求超时的时间5s
  timeout: 5000,
})

//请求拦截器:在发送请求之前,请求拦截器可以检测到,可以在请求发出去之前做一些事情
//interceptors拦截器 
//request请求的信息
requests.interceptors.request.use((config)=>{
  // console.log(store) 测试store是否引入成功
  if(store.state.detail.uuid_token){
    //请求头添加一个字段(userTempId):这个名字是和后台的老师商量好了的
    config.headers.userTempId = store.state.detail.uuid_token
  }
  //需要携带token给服务器
  if(store.state.user.token){
    config.headers.token = store.state.user.token
  }
  //当请求拦截器捕获到请求的时候,进度条开始动
  nprogress.start()
  //config:配置对象,对象里面有一个属性很重要,headers请求头
  return config;
})


//响应拦截器
//response响应的信息
//成功的回调会返回服务器响应的数据res
//失败的回调error
requests.interceptors.response.use((res)=>{
  //当服务器返回的数据成功了,你拿到数据的时候,进度条结束
  nprogress.done()
  //成功的回调函数;服务器响应的数据回来以后,响应拦截器可以检测到,可以做一些事情
  return res.data
},(error)=>{
  //响应失败的回调函数
  return Promise.reject(new Error('faile')) //终结Promise链  当然也可以打印错误信息console.log()
})



//对外暴露
export default requests;