//当前这个模块:API进行统一管理

//你要发请求,需要用到我们封装的axios
//引入我们封装的aixos
import requests from './request'
import mockRequests from './mockAjax'
//三级联动的接口
///请求地址: api/product/getBaseCategoryList   get  无参数

export const reqCategoryList = ()=>requests({  //对外暴露
  url:'/product/getBaseCategoryList',  //因为在request.js中我们用了baseURL基础路径设置为/api,所以这里url我们就可以不用加api了
  method:'get'
})

//获取banner(Home首页轮播图接口)
export const reqGetBannerList = ()=>mockRequests.get('/banner')

//获取floor数据
export const reqFloorList = ()=>mockRequests.get('/floor')

//获取搜索search模块数据  地址:/api/list  请求的方式:post
//参数:
/* 
{
  "category3Id": "61",
  "categoryName": "手机",
  "keyword": "小米",
  "order": "1:desc",
  "pageNo": 1,
  "pageSize": 10,
  "props": ["1:1700-2799:价格", "2:6.65-6.74英寸:屏幕尺寸"],
  "trademark": "4:小米"
}
*/
//当前这个接口(获取搜索模块的数据),给服务器传递一个默认参数[至少是一个空对象]
export const reqGetSearchInfo = (params)=>requests({url:"/list",method:"post",data:params})

//获取产品详情信息detail的接口  URL: /api/item/{ skuId } 请求方式:get
//这里发请求,要传参,带的就是skuId
export const reqGoodsInfo = (skuId)=>requests({url:`/item/${skuId}`,method:'get'})

//将产品添加到购物车中(获取更新某一个产品的个数)
//URL:  /api/cart/addToCart/{skuId}/{skuNum}  请求方式:post
export const reqAddOrUpdateShopCart = (skuId,skuNum)=>requests({url:`/cart/addToCart/${skuId}/${skuNum}`,method:'post'})

//获取购物车列表数据接口
//URL:  /api/cart/cartList    请求方式:get
export const reqCartList = ()=>requests({url:'/cart/cartList',method:'get'})

//删除购物车产品的接口
//URL: /api/cart/deleteCart/{skuId}  请求方式:delete
export const reqDeleteCartById = (skuId)=>requests({url:`/cart/deleteCart/${skuId}`,method:'delete'})

//修改商品的选中状态
//URL: /api/cart/checkCart/{skuId}/{isChecked} 请求方式:get
export const reqUpdeteCheckedByid = (skuId,isChecked)=>requests({url:`/cart/checkCart/${skuId}/${isChecked}`,method:'get'})

//获取验证码
//URL: /api/user/passport/sendCode/{phone}  请求方式:get
export const reqGetCode = (phone)=>requests({url:`/user/passport/sendCode/${phone}`,method:'get'})

//注册
//URL: /api/user/passport/register  请求方式:post  参数phone code password
//带的参数是三个,我们(data)是一个对象,来代替那三个参数
//在url中路径没有提示带参的时候,我们用data来带
export const reqUserRegister = (data)=>requests({url:'/user/passport/register',data,method:'post'})

//登录
//URL: /api/user/passport/login  请求方式:post  参数phone  password
export const reqUserLogin = (data)=>requests({url:'user/passport/login',data,method:'post'})

//获取用户信息[需要带用户的token向服务器要用户信息]
//URL: /api/user/passport/auth/getUserInfo  请求方式:get  
//因为这里需要带参数给服务器,但是我们的url中没有写多余参数,所以这里我们利用请求头把token带给服务器
//上面的登录和注册是post请求,参数用data传,这里是get请求,用请求头传
export const reqUserInfo = ()=>requests({url:'/user/passport/auth/getUserInfo',method:'get'})

//退出登录
//URL: /api/user/passport/logout 请求方式:get 没有参数
export const reqLogout = ()=>requests({url:'/user/passport/logout',method:'get'})

//获取用户地址信息
//URL:/api/user/userAddress/auth/findUserAddressList 请求方式:get 没有参数
export const reqAddressInfo = ()=>requests({url:'/user/userAddress/auth/findUserAddressList',method:'get'})

//获取商品清单
//URL:/api/order/auth/trade  method:get
export const reqOrderInfo = ()=>requests({url:'/order/auth/trade',method:'get'})

//提交订单
//URL:/api/order/auth/submitOrder?tradeNo={tradeNo}  请求方式:post 参数7个,tradeNo通过query带,还有6个参数,用data代替
export const reqSubmitOrder = (tradeNo,data)=>requests({url:`/order/auth/submitOrder?tradeNo=${tradeNo}`,data,method:'post'})

//获取支付信息
//URL: /api/payment/weixin/createNative/{orderId} 请求方式:get 
export const reqPayInfo = (orderId)=>requests({url:`/payment/weixin/createNative/${orderId}`,method:'get'})

//获取支付订单状态
//URL:/api/payment/weixin/queryPayStatus/{orderId} 请求方式:get
export const reqPayStatus = (orderId)=>requests({url:`/payment/weixin/queryPayStatus/${orderId}`,method:'get'})

//获取个人中心的数据
//URL: /api/order/auth/{page}/{limit}    请求方式:get
export const reqMyOrderList = (page,limit)=>requests({url:`/order/auth/${page}/${limit}`,method:'get'})