//引入uuid   as这种引法是起别名
import { v4 as uuidv4 } from 'uuid';
//要生成一个随机的字符串,且每次执行不能发生变化,游客身份持久化存储
export const getUUID = ()=>{
  //先从本地存储获取uuid(看一下本地存储里面是否有)
  let uuid_token = localStorage.getItem('UUIDTOKEN')
  //如果没有
  if(!uuid_token){
    //我生成游客临时身份
    uuid_token = uuidv4()
    //本地存储存储一次
    localStorage.setItem('UUIDTOKEN',uuid_token)//如果没有我就把生成的uuid_token存储在本地的UUIDTOKEN中
  }
  return uuid_token
}