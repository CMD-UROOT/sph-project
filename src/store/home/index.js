//引入我们封装的api
import { reqCategoryList, reqGetBannerList,reqFloorList } from "@/api"

//home模块的小仓库
//state:仓库存储数据的地方,也就是状态
const state = {
  //state中的数据默认初始值别乱写,服务器返回的是对象,服务器返回的是数组,[初始值是根据接口返回值去初始化的]
  //home仓库中存储三级菜单的数据
  categoryList:[], //初始值
  //轮播图的数据
  bannerList:[],
  //floor的数据
  floorList:[]
}
//mutations:修改state的唯一手段
const mutations = {
  CATEGORYLIST(state,categoryList){
    state.categoryList = categoryList  //state.categoryList 等于服务器返回的categoryList
  },
  GETBANNERLIST(state,bannerList){
    // console.log('修改仓库当中的数据')
    state.bannerList = bannerList
  },
  GETFLOORLIST(state,floorList){
    state.floorList = floorList
  }
}
//actions:处理actions,可以书写自己的业务逻辑,也可以处理异步
const actions = {
  //通过API里面的接口函数调用,向服务器发请求,获取服务器的数据
  async categoryList({commit}){ //{commit}结构出commit提交mutation
    let result = await reqCategoryList()  //为了拿到Promise的成功的结果所以这里加await, async和await是cp,必须同时存在
    //console.log(result)//不加await和async返回的结果是Promise//加了await和async返回的结果是成功的结果,就是我们三级联动的数据了
    if(result.code==200){//如果结果为200 
      commit("CATEGORYLIST",result.data)//把CATEGORYLIST提交mutation   result.data是提交的数据
    }
  },
  //获取首页轮播图的数据
  async getBannerList({commit}){
    // console.log('获取服务器数据')
    let result = await reqGetBannerList()  
    // console.log(result) 控制台查看数据是否获取成功
    if(result.code==200){
      commit('GETBANNERLIST',result.data)
    }
  },
  //获取floor数据
  async getFloorList({commit}){
    let result = await reqFloorList()
    if(result.code==200){
      //提交mutation
      commit("GETFLOORLIST",result.data)
    }
  }
}


//getters:理解为计算属性,用于简化仓库数据,让组件获取仓库的数据更加方便
const getters = {}

//对外暴露Store类的一个实例
export default{
  state,
  mutations,
  actions,
  getters
}