//引入api
import { reqGetSearchInfo } from "@/api"
//search模块的小仓库
//state:仓库存储数据的地方,也就是状态
const state = {
  //仓库初始状态
  searchList:{}  //这里是{}还是[]要派发数据以后,去控制台打印查看network
}
//mutations:修改state的唯一手段
const mutations = {
  GETSEARCHLIST(state,searchList){
    state.searchList = searchList
  }
}
//actions:处理actions,可以书写自己的业务逻辑,也可以处理异步
const actions = {
  //获取search模块数据
  async getSearchList({commit},params={}){  //params={}表示默认参数,外部调用这个函数,params传了,就用你传的,没传就用默认的{}
    //当前这个reqGetSearchInfo这个函数在调用获取服务器数据的时候,至少传递一个参数(空对象)
    //params形参,是当用户派发action的时候,第二个参数传递过来的,至少是一个空对象
    let result = await reqGetSearchInfo(params)
    // console.log(result)  这里派发action后可以看到打印结果
    if(result.code==200){
      commit('GETSEARCHLIST',result.data)//提交mutation
    }

  }
}
//getters:理解为计算属性,用于简化仓库数据,让组件获取仓库的数据更加方便
//可以把我们将来在组件当中需要的数据简化一下[将来组件在获取数据的时候就方便了]
const getters = {
  //当前形参state,是当前仓库的state,并非大仓库中的那个state
  goodsList(state){
    //state.searchList.goodsList如果服务器数据回来了,没问题是一个数组
    //假如网络不给力|没有网 state.searchList.goodsList应该返回的是undefined
    //相当于我们state中定义的searchList:{}.goodsList是undefined,那边pages/search还遍历了,undefined不可能遍历,至少是个数组才能遍历,所以计算属性的属性值至少来一个数组
    return state.searchList.goodsList||[]
  },
  trademarkList(state){
    return state.searchList.trademarkList
  },
  attrsList(state){
    return state.searchList.attrsList
  }
}

//对外暴露Store类的一个实例
export default{
  state,
  mutations,
  actions,
  getters
}